# Car-Api

## Requirements
NodeJS v16 or higher
Docker v19.03.0 or higher

## Install
install with `npm install`

## Usage
start server with docker compose up

server is accessible on http://localhost:3000

endpoints available for use:

GET/POST /api/car

GET/PATCH/DELETE /api/car/:id

see tests for examples.

## Development
create a .env file using .env.example as a template.

`npm run dev` provides hot-reloading with nodemon

## Tests
run tests with `npm test`
