import supertest from 'supertest';
import createApp from '../../src/app';
import { Container } from 'typedi';
import { Connection } from 'mongoose';

describe('/api/car endpoint', () => {
  let app;
  let carModel;
  const id = '5c0a7922c9d89830f4911426';

  const exampleCar = {
    color: 'blue',
    isConvertible: true,
    make: 1998,
    model: 'clio',
    brand: 'nissan',
  };

  beforeEach(async () => {
    app = await createApp();
    carModel = Container.get('carModel');
    await carModel.deleteMany();
  });

  afterAll(async () => {
    const db: Connection = Container.get('db');
    await db.close();
  });

  describe('POST /api/car', () => {
    it('should create a new car', async () => {
      const carToCreate = {
        color: 'blue',
        isConvertible: true,
        make: 1998,
        model: 'something',
        brand: 'create_test_car',
      };
      const response = await supertest(app).post('/api/car').send(carToCreate);
      expect(response.body).toEqual({ message: 'car created' });

      const carInDB = await carModel.findOne(carToCreate);

      expect(carInDB).toEqual(expect.objectContaining(carToCreate));
    });

    it('should require all necessary params', async () => {
      const response = await supertest(app).post('/api/car');
      expect(response.body).toEqual([
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'color'",
          params: { missingProperty: 'color' },
          schemaPath: '#/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'brand'",
          params: { missingProperty: 'brand' },
          schemaPath: '#/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'model'",
          params: { missingProperty: 'model' },
          schemaPath: '#/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'make'",
          params: { missingProperty: 'make' },
          schemaPath: '#/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'isConvertible'",
          params: { missingProperty: 'isConvertible' },
          schemaPath: '#/required',
        },
      ]);
    });
  });

  describe('GET /api/car', () => {
    it('should get all cars', async () => {
      const carsToCreate = [exampleCar, { ...exampleCar, color: 'brown' }, { ...exampleCar, color: 'green' }];
      await carModel.create(carsToCreate);

      const response = await supertest(app).get('/api/car');

      expect(response.body).toHaveLength(carsToCreate.length);
      carsToCreate.forEach(car => {
        expect(response.body).toContainEqual(expect.objectContaining(car));
      });
    });
  });

  describe('GET /api/car/:id', () => {
    it('should get one car', async () => {
      const carToGet = { ...exampleCar, _id: id };
      await carModel.create(carToGet);

      const response = await supertest(app).get(`/api/car/${id}`);
      expect(response.body).toEqual(expect.objectContaining(carToGet));
    });

    it('should require valid params', async () => {
      const carToGet = { ...exampleCar, _id: id };
      await carModel.create(carToGet);

      const response = await supertest(app).get('/api/car/something');
      expect(response.body).toEqual([
        {
          instancePath: '/id',
          keyword: 'errorMessage',
          message: 'id must be an mongodb ObjectId',
          params: {
            errors: [
              {
                emUsed: true,
                instancePath: '/id',
                keyword: 'pattern',
                message: 'must match pattern "^[a-f\\d]{24}$"',
                params: { pattern: '^[a-f\\d]{24}$' },
                schemaPath: '#/properties/id/pattern',
              },
            ],
          },
          schemaPath: '#/properties/id/errorMessage',
        },
      ]);
    });
  });

  describe('DELETE /api/car/:id', () => {
    it('should delete one car', async () => {
      const carToDelete = { ...exampleCar, _id: id };
      await carModel.create(carToDelete);
      const response = await supertest(app).delete(`/api/car/${id}`);
      expect(response.body).toEqual({ message: 'car with id: 5c0a7922c9d89830f4911426 deleted' });

      const carInDB = await carModel.findOne(carToDelete);
      expect(carInDB).toBeNull();
    });

    it('should require valid params', async () => {
      const carToGet = { ...exampleCar, _id: id };
      await carModel.create(carToGet);

      const response = await supertest(app).delete('/api/car/something');
      expect(response.body).toEqual([
        {
          instancePath: '/id',
          keyword: 'errorMessage',
          message: 'id must be an mongodb ObjectId',
          params: {
            errors: [
              {
                emUsed: true,
                instancePath: '/id',
                keyword: 'pattern',
                message: 'must match pattern "^[a-f\\d]{24}$"',
                params: { pattern: '^[a-f\\d]{24}$' },
                schemaPath: '#/properties/id/pattern',
              },
            ],
          },
          schemaPath: '#/properties/id/errorMessage',
        },
      ]);
    });
  });

  describe('PATCH /api/car/:id', () => {
    it('should patch one car', async () => {
      const carToPatch = { ...exampleCar, _id: id };
      const patch = {
        color: 'red',
      };
      await carModel.create(carToPatch);
      const response = await supertest(app).patch(`/api/car/${id}`).send(patch);
      expect(response.body).toMatchObject({ ...carToPatch, ...patch });
    });

    it('should require valid params', async () => {
      const carToPatch = { ...exampleCar, _id: id };
      await carModel.create(carToPatch);
      const patch = {
        color: 'red',
      };
      const response = await supertest(app).patch('/api/car/something').send(patch);
      expect(response.body).toEqual([
        {
          instancePath: '/id',
          keyword: 'errorMessage',
          message: 'id must be an mongodb ObjectId',
          params: {
            errors: [
              {
                emUsed: true,
                instancePath: '/id',
                keyword: 'pattern',
                message: 'must match pattern "^[a-f\\d]{24}$"',
                params: { pattern: '^[a-f\\d]{24}$' },
                schemaPath: '#/properties/id/pattern',
              },
            ],
          },
          schemaPath: '#/properties/id/errorMessage',
        },
      ]);
    });

    it('should at least one prop to patch', async () => {
      const carToPatch = { ...exampleCar, _id: id };
      await carModel.create(carToPatch);
      const patch = {};
      const response = await supertest(app).patch(`/api/car/${id}`).send(patch);
      expect(response.body).toEqual([
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'color'",
          params: {
            missingProperty: 'color',
          },
          schemaPath: '#/oneOf/0/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'brand'",
          params: {
            missingProperty: 'brand',
          },
          schemaPath: '#/oneOf/1/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'model'",
          params: {
            missingProperty: 'model',
          },
          schemaPath: '#/oneOf/2/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'make'",
          params: {
            missingProperty: 'make',
          },
          schemaPath: '#/oneOf/3/required',
        },
        {
          instancePath: '',
          keyword: 'required',
          message: "must have required property 'isConvertible'",
          params: {
            missingProperty: 'isConvertible',
          },
          schemaPath: '#/oneOf/4/required',
        },
        {
          instancePath: '',
          keyword: 'oneOf',
          message: 'must match exactly one schema in oneOf',
          params: {
            passingSchemas: null,
          },
          schemaPath: '#/oneOf',
        },
      ]);
    });
  });
});
