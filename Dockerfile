FROM node:16 as builder

COPY --chown=node:node . /usr/src/app

# Create app directory
WORKDIR /usr/src/app

# Copy all source code to work directory
ADD . .

# Install all Packages
RUN npm install

# build app
RUN npm run build

FROM node:16 as runner

ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV

# Create app directory
WORKDIR /usr/src/app

COPY --from=builder ./usr/src/app/build .

COPY package* ./

RUN npm ci

# expose app port
EXPOSE 3000

USER node

CMD [ "node", "index.js" ]
