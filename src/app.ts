import 'reflect-metadata'; // We need this in order to use @Decorators
import express from 'express';

import loaders from './loaders';

export default async function createApp(): Promise<express.Application> {
  const app = express();
  await loaders({ expressApp: app });

  return app;
}
