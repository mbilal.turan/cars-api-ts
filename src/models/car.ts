import { ICar } from '../interfaces/ICar';
import mongoose from 'mongoose';
const Car = new mongoose.Schema(
  {
    color: {
      type: String,
      required: true,
    },
    brand: {
      type: String,
      required: true,
      index: true,
    },
    model: {
      type: String,
      required: true,
    },
    make: {
      type: Number,
      required: true,
    },
    isConvertible: {
      type: Boolean,
      required: true,
    },
  },
  { timestamps: true },
);

export default mongoose.model<ICar & mongoose.Document>('Car', Car);
