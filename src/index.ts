import createApp from './app';
import Logger from './loaders/logger';

async function startServer() {
  const app = await createApp();

  const PORT = 3000;
  app
    .listen(PORT, () => {
      Logger.info(`Server listening on port: ${PORT}`);
    })
    .on('error', err => {
      Logger.error(err);
      process.exit(1);
    });
}

startServer();
