import expressLoader from './express';
import mongooseLoader from './mongoose';
import Logger from './logger';
import CarModel from '../models/car';
import dependencyInjectorLoader from './dependency-injector';
import express from 'express';
export default async ({ expressApp }: { expressApp: express.Application }): Promise<void> => {
  const mongoConnection = await mongooseLoader();
  Logger.info('DB loaded and connected!');

  const carModel = {
    name: 'carModel',
    model: CarModel,
  };

  dependencyInjectorLoader({
    mongoConnection,
    models: [carModel],
  });

  await expressLoader({ app: expressApp });
  Logger.info('Express loaded');
};
