import { Container } from 'typedi';
import LoggerInstance from './logger';
import { Document, Model } from 'mongoose';
import { ICar } from '../interfaces/ICar';

export default ({
  mongoConnection,
  models,
}: {
  mongoConnection;
  models: { name: string; model: Model<ICar & Document> }[];
}): void => {
  try {
    Container.set('db', mongoConnection);

    models.forEach(m => {
      Container.set(m.name, m.model);
    });

    Container.set('logger', LoggerInstance);
  } catch (e) {
    LoggerInstance.error('Error on dependency injector loader: %o', e);
    throw e;
  }
};
