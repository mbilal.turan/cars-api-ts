import { Router } from 'express';
import car from './routes/car';

export default (): Router => {
  const app = Router();
  car(app);

  return app;
};
