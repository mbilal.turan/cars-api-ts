import { Container } from 'typedi';
import CarModel from '../../../models/car';
import { Request, Response } from 'express';
export const getCarByIdParamsSchema = {
  type: 'object',
  properties: {
    id: { type: 'string', pattern: '^[a-f\\d]{24}$', errorMessage: { pattern: 'id must be an mongodb ObjectId' } },
  },
  required: ['id'],
  additionalProperties: false,
};

export async function getByIdController(req: Request, res: Response): Promise<void> {
  const carId = req.params.id;
  const carModel: typeof CarModel = Container.get('carModel');

  const result = await carModel.findById(carId).lean();

  if (!result) {
    res.status(404).json({ message: `car with id: ${carId} not found` });
    return;
  }
  res.status(200).json(result);
}
