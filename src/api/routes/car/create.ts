import { Container } from 'typedi';
import { ICarDTO } from '../../../interfaces/ICar';
import CarModel from '../../../models/car';
import { Request, Response } from 'express';
export const createCarDTOSchema = {
  type: 'object',
  properties: {
    color: { type: 'string' },
    brand: { type: 'string' },
    model: { type: 'string' },
    make: { type: 'integer' },
    isConvertible: { type: 'boolean' },
  },
  required: ['color', 'brand', 'model', 'make', 'isConvertible'],
  additionalProperties: false,
};

export async function createCarController(req: Request, res: Response): Promise<void> {
  const newCar = req.body as ICarDTO;
  const carModel: typeof CarModel = Container.get('carModel');

  await carModel.create(newCar);
  res.status(201).json({ message: 'car created' });
}
