import { Container } from 'typedi';
import CarModel from '../../../models/car';
import { Request, Response } from 'express';
import { ICarDTO } from '../../../interfaces/ICar';
export const patchCarByIdParamsSchema = {
  type: 'object',
  properties: {
    id: { type: 'string', pattern: '^[a-f\\d]{24}$', errorMessage: { pattern: 'id must be an mongodb ObjectId' } },
  },
  required: ['id'],
  additionalProperties: false,
};

export const patchCarByIdBodySchema = {
  type: 'object',
  properties: {
    color: { type: 'string' },
    brand: { type: 'string' },
    model: { type: 'string' },
    make: { type: 'integer' },
    isConvertible: { type: 'boolean' },
  },
  oneOf: [
    { required: ['color'] },
    { required: ['brand'] },
    { required: ['model'] },
    { required: ['make'] },
    { required: ['isConvertible'] },
  ],
  additionalProperties: false,
};

export async function patchCarByIdController(req: Request, res: Response): Promise<void> {
  const carId = req.params.id;
  const carProps = req.body as ICarDTO;
  const carModel: typeof CarModel = Container.get('carModel');

  const result = await carModel.findByIdAndUpdate(carId, carProps, { new: true });
  if (!result) {
    res.status(404).json({ message: `car with id: ${carId} not found` });
    return;
  }
  res.status(200).json(result);
}
