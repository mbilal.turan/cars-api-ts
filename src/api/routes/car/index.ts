import { Router } from 'express';
import { createValidationMiddleware, ValidationProps } from '../../middlewares/validation';
import { createCarController, createCarDTOSchema } from './create';
import { getAllController } from './get-all';
import { getByIdController, getCarByIdParamsSchema } from './get-by-id';
import { deleteByIdController, deleteCarByIdSchema } from './delete-by-id';
import { patchCarByIdBodySchema, patchCarByIdParamsSchema, patchCarByIdController } from './patch-by-id';

export default (app: Router): void => {
  const route = Router();

  app.use('/car', route);
  route.patch(
    '/:id',
    createValidationMiddleware(patchCarByIdParamsSchema, ValidationProps.PARAMS),
    createValidationMiddleware(patchCarByIdBodySchema, ValidationProps.BODY),
    patchCarByIdController,
  );

  route.delete('/:id', createValidationMiddleware(deleteCarByIdSchema, ValidationProps.PARAMS), deleteByIdController);
  route.get('/:id', createValidationMiddleware(getCarByIdParamsSchema, ValidationProps.PARAMS), getByIdController);
  route.get('/', getAllController);
  route.post('/', createValidationMiddleware(createCarDTOSchema, ValidationProps.BODY), createCarController);
};
