import { Container } from 'typedi';
import CarModel from '../../../models/car';
import { Request, Response } from 'express';

export async function getAllController(req: Request, res: Response): Promise<void> {
  const carModel: typeof CarModel = Container.get('carModel');

  const result = await carModel.find().lean();
  res.status(200).json(result);
}
