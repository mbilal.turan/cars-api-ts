import Ajv, { Schema } from 'ajv';
import { RequestHandler } from 'express';
import ajvErrors from 'ajv-errors';
export enum ValidationProps {
  PARAMS = 'params',
  BODY = 'body',
}

export function createValidationMiddleware(schema: Schema, validationPropName: ValidationProps): RequestHandler {
  const ajv = new Ajv({ allErrors: true });
  ajvErrors(ajv);
  const validate = ajv.compile(schema);
  return (req, res, next) => {
    if (validate(req[validationPropName])) {
      next();
    } else {
      res.status(400).json(validate.errors);
    }
  };
}
