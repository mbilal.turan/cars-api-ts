export interface ICar {
  _id: string;
  color: string;
  brand: string;
  model: string;
  make: number;
  isConvertible: boolean;
}

export interface ICarDTO {
  color: string;
  brand: string;
  model: string;
  make: number;
  isConvertible: boolean;
}
