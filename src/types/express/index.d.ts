import { Document, LeanDocument, Model } from 'mongoose';
import { ICar } from '../../interfaces/ICar';
declare global {
  namespace Express {
    export interface Request {
      currentUser: LeanDocument<ICar>;
      token: { _id: string };
    }
  }

  namespace Models {
    export type CarModel = Model<ICar & Document>;
  }
}
